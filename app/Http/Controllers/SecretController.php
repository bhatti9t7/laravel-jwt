<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SecretController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function default()
    {
        return response()->json(["name"=>"Abdullah"]);
    }

}
